const expect = require('chai').expect
const converter = require('../src/converter')

describe("Color code converter", ()=>{
    describe("RGB to Hex conversion", ()=>{
        it("Converts the basic colors", ()=>{
            const redHex = converter.rgbToHex(255, 0, 0)
            const greenHex = converter.rgbToHex(0,255,0)
            const blueHex = converter.rgbToHex(0,0,255)

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })

    describe("Hex to RGB", () =>{
        const redRGB = converter.hexToRGB("ff0000")
        const greenRGB = converter.hexToRGB("00ff00")
        const blueRGB = converter.hexToRGB("0000ff")

        expect(redRGB).to.equal("255,000,000")
        expect(greenRGB).to.equal("000,255,000")
        expect(blueRGB).to.equal("000,000,255")
    })
})
