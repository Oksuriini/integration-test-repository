const pad = (hex) =>{
    return(hex.length === 1 ? "0"+hex : hex)
}
const karhuherrapaddington = (color) =>{
    switch (color.length) {
        case 1:
            return "0"+"0"+String(color)
        case 2:
            return "0"+String(color)
        case 3:
            return String(color)

        default:
            console.log("Jottain männöös pahhaasti pielehe")
            return ":DDD"
    }
}


module.exports={
    rgbToHex: (red,green,blue)=>{
        const redHex = red.toString(16)
        const greenHex = green.toString(16)
        const blueHex = blue.toString(16)

        return pad(redHex) + pad(greenHex) + pad(blueHex)
    },
    hexToRGB: (hex) => {
        const red = String(parseInt((hex[0] + hex[1]),16))
        const green = String(parseInt((hex[2] + hex[3]),16))
        const blue = String(parseInt((hex[4] + hex[5]),16))
        
        return (karhuherrapaddington(red) + ","+ karhuherrapaddington(green)+ "," +karhuherrapaddington(blue))
    }
}